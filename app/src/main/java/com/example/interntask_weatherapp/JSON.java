package com.example.interntask_weatherapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JSON {
    @GET("?query=ba")
    Call<List<Post>> getPost();
}
