package com.example.interntask_weatherapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> implements Filterable  {

    List<Post> postList;
    Context context;
    List<Post> filteredList;
    List<Post> selectedList;

    public void setList(Context context, final List<Post> postList){
        this.context = context;
        if(this.postList == null){
            this.postList = postList;
            this.filteredList = postList;
            this.selectedList = postList;

            notifyItemChanged(0, filteredList.size());
        } else {
            final DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return PostAdapter.this.postList.size();
                }

                @Override
                public int getNewListSize() {
                    return postList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return PostAdapter.this.postList.get(oldItemPosition).getTitle().equals(postList.get(newItemPosition).getTitle());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {

                    Post newCity = PostAdapter.this.postList.get(oldItemPosition);

                    Post lodCity = postList.get(newItemPosition);

                    return newCity.getTitle().equals(lodCity.getTitle()) ;
                }
            });
            this.postList = postList;
            this.filteredList = postList;
            result.dispatchUpdatesTo(this);
        }
    }


    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.unit, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {


        holder.get_title.setText("Title: " + filteredList.get(position).getTitle());
        holder.get_loc_type.setText("Location Type: " + filteredList.get(position).getLocation_type());
        holder.get_woeid.setText("Woeid: " + filteredList.get(position).getWoeid());
        holder.get_lat_long.setText("Latitude & Longitude: " + filteredList.get(position).getLatt_long());

        holder.checkBox.setOnCheckedChangeListener(null);

        //Check Box
        //Check if the item is checked or not from the model.
        if(filteredList.get(holder.getAbsoluteAdapterPosition()).isSelected())
            holder.checkBox.setChecked(true);
        else
            holder.checkBox.setChecked(false);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    filteredList.get(holder.getAbsoluteAdapterPosition()).setSelected(true);
                }
                else {
                    filteredList.get(holder.getAbsoluteAdapterPosition()).setSelected(false);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if(postList != null){
            return filteredList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = postList;
                } else {
                    List<Post> filteredlst = new ArrayList<>();
                    for (Post post : postList) {
                        if (post.getTitle().toLowerCase().contains(charString.toLowerCase())) {
                            filteredlst.add(post);
                        }
                    }
                    filteredList = filteredlst;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Post>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder{

        TextView get_title, get_loc_type, get_woeid, get_lat_long;
        CheckBox checkBox;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);

            get_title = itemView.findViewById(R.id.get_title);
            get_loc_type = itemView.findViewById(R.id.get_loc_type);
            get_woeid = itemView.findViewById(R.id.get_woeid);
            get_lat_long = itemView.findViewById(R.id.get_lat_long);
            checkBox = itemView.findViewById(R.id.checkBox);



        }
    }

}
