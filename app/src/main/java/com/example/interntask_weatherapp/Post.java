package com.example.interntask_weatherapp;

public class Post {

    private String title,location_type, woeid, latt_long;
    private boolean isSelected;

    public String getTitle() {
        return title;
    }

    public String getLocation_type() {
        return location_type;
    }

    public String getWoeid() {
        return woeid;
    }

    public String getLatt_long() {
        return latt_long;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

}
